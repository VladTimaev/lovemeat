import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../pages/Home.vue'
import ProductItem from '../pages/Product.vue'
import ProductCategory from '../pages/ProductCategory.vue'
import Cart from '../pages/Cart.vue'
import Recipes from '../pages/Recipes.vue'
import RecipesDetails from '../pages/RecipesDetails.vue'
import Blog from '../pages/Blog.vue'
import BlogDetails from '../pages/BlogDetails.vue'
import Reviews from '../pages/Reviews.vue'
import Quality from '../pages/Quality.vue'
import About from '../pages/About.vue'
import PaymentDelivery from '../pages/PaymentDelivery.vue'
import Favorite from '../pages/Favorite.vue'
import Contacts from '../pages/Contacts.vue'
import Checkout from '../pages/Checkout.vue'
import PersonalAccount from '../pages/PersonalAccount.vue'
import CheckoutReport from '../pages/CheckoutReport.vue'
import Order from '../pages/Order.vue'
import Search from '../pages/Search.vue'
import PrivacyPolicy from '../pages/PrivacyPolicy.vue'
import PasswordRecovery from '../pages/PasswordRecovery.vue'
import PageNotFound from '../pages/PageNotFound.vue' 

import { RouteName } from './utils'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: RouteName.Home,
    component: Home,
  },
  {
    path: '/product/:slug'  ,
    name: RouteName.Product,
    component: ProductItem,
    meta: {
      keepAlive: false
    }
  },
  {
    path: '/product-category/:slug'  ,
    name: RouteName.CollectionByCategory,
    component: ProductCategory,
    meta: {
      keepAlive: false
    }
  },
  {
    path: '/cart'  ,
    name: RouteName.Cart,
    component: Cart,
    meta: {
      keepAlive: false
    }
  },
  {
    path: '/recipes',
    name: RouteName.Recipes,
    component: Recipes,
    meta: {
      keepAlive: true
    }
  },
  {
    path: '/recipes/:id',
    name: RouteName.RecipesDetails,
    component: RecipesDetails,
    meta: {
      keepAlive: false
    }
  },
  {
    path: '/blog',
    name: RouteName.Blog,
    component: Blog,
    meta: {
      keepAlive: true
    }
  },
  {
    path: '/blog/:id',
    name: RouteName.BlogDetails,
    component: BlogDetails,
    meta: {
      keepAlive: false
    }
  },
  {
    path: '/reviews',
    name: RouteName.Reviews,
    component: Reviews,
    meta: {
      keepAlive: true
    }
  },
  {
    path: '/quality',
    name: RouteName.Quality,
    component: Quality,
    meta: {
      keepAlive: true
    }
  },
  {
    path: '/about',
    name: RouteName.About,
    component: About,
    meta: {
      keepAlive: true
    }
  },
  {
    path: '/payment-delivery',
    name: RouteName.PaymentDelivery,
    component: PaymentDelivery,
    meta: {
      keepAlive: true
    }
  },
  {
    path: '/favorite',
    name: RouteName.Favorite,
    component: Favorite,
    meta: {
      keepAlive: false
    }
  },
  {
    path: '/contacts',
    name: RouteName.Contacts,
    component: Contacts,
    meta: {
      keepAlive: true
    }
  },
  {
    path: '/checkout',
    name: RouteName.Checkout,
    component: Checkout,
    meta: {
      keepAlive: false
    }
  },
  {
    path: '/personal-account',
    name: RouteName.PersonalAccount,
    component: PersonalAccount,
    meta: {
      keepAlive: false
    }
  },
  {
    path: '/checkout/report',
    name: RouteName.CheckoutReport,
    component: CheckoutReport,
    meta: {
      keepAlive: false
    }
  },
  {
    path: '/order/:id',
    name: RouteName.Order,
    component: Order,
    meta: {
      keepAlive: false
    }
  },
  {
    path: '/search',
    name: RouteName.Search,
    component: Search,
    meta: {
      keepAlive: false
    }
  },
  {
    path: '/privacy-policy',
    name: RouteName.PrivacyPolicy,
    component: PrivacyPolicy,
    meta: {
      keepAlive: true
    }
  },
  {
    path: '/password-recovery',
    name: RouteName.PasswordRecovery,
    component: PasswordRecovery
  },
  {
    path: '*',
    name: RouteName. PageNotFound,
    component: PageNotFound
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
