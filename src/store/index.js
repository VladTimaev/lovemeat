import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import persistedStateIndexDB from '../plugins/persistedStateIndexDB'
import persistedState from '../plugins/persistedState'

/* import modules */
import menu from './modules/menu'
import search from './modules/search'
import product from './modules/product'
import cart from './modules/cart'
import favorite from './modules/favorite'
import modal from './modules/modal'
import user from './modules/user'
import checkout from './modules/checkout'
import account from './modules/account'
import review from './modules/review'


export default new Vuex.Store({
  modules: {
    menu,
    search,
    product,
    cart,
    favorite,
    modal,
    user,
    checkout,
    account,
    review
  },
  plugins:[persistedStateIndexDB, persistedState ]
})
