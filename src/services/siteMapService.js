import axios from 'axios'

const getXmlFile  = (url) => {
  const api = axios
  const xml2js = require('xml2js');

  api.get(url).then((response) => {

    xml2js.parseString(response.data, (err, result) => {
      if(err) {
          throw err;
      }

      const json = JSON.stringify(result, null);

      const array = JSON.parse(json)

      array['urlset'].url.forEach(item => {
        item.loc[0] = `https://lovemeat.ru${item.loc[0].split('https://back.lovemeat.ru')[1]}`
      })

      const arrayJson = JSON.stringify(array)
      console.log(arrayJson)
      return arrayJson
   });
  })
}

export default {
  getXmlFile
}