import menuApiService from '@/api/menuApiService'
import productApiService from '@/api/productApiService'
import pageApiService from '@/api/pageApiService'
import productService from '@/services/productService'
import jsonService from '@/services/jsonService'
import contactApiService from '@/api/contactApiService'
import orderApiService from '@/api/orderApiService'
import dateService from './dateService'
import accountApiService from '../api/accountApiService'
import favoriteApiService from '../api/favoriteApiService'
import reviewService from './reviewService'
import siteMapService from './siteMapService'
import userApiService  from '../api/userApiService'
import profileService from './profileService'

export default {
  menuApiService,
  productApiService,
  productService,
  jsonService,
  pageApiService,
  contactApiService,
  orderApiService,
  dateService,
  accountApiService,
  favoriteApiService,
  reviewService,
  siteMapService,
  userApiService,
  profileService
}