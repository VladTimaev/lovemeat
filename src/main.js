import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import serviceContainer from './services/serviceContainer'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import './assets/scss/main.scss'

import VueMeta from 'vue-meta'
import VueYandexMetrika from 'vue-yandex-metrika'                               
 
Vue.use(VueYandexMetrika, {
    id: 88716742,
    env:'production'
})

Vue.use(VueMeta, {
  refreshOnceOnNavigation: true
})

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  provide: serviceContainer,
  render: h => h(App)
}).$mount('#app')
